from selenium import webdriver
import sys

from src.emagpagetwo import seleniumLogic

print("Usage is:", "python runnerpagetwo.py NO_OF_PAGES LOG_LEVEL")

try:
    no_of_pages = int(sys.argv[1])
    log_level = sys.argv[2]
except Exception as e:
    no_of_pages = 2
    log_level = None
if no_of_pages == None:
    no_of_pages = 2

duration = 2 #seconds
#duration == 2000 #milliseconds for windows
freq = 440  # Hz
# driver = webdriver.Chrome()

driver = webdriver.PhantomJS()
driver.set_window_size(1280,1080)
driver.set_page_load_timeout(60)

base_url = "https://www.emag.ro"
url_login = base_url + "/user/login?ref=hdr_login_btn"
url_jocuri = base_url + "/search/jocuri-consola-pc/xbox/c"
url_console = base_url + "/search/console-hardware/xbox/sort-priceasc/c"
url_oled = base_url + "/televizoare/filter/diagonala-ecran-cm-f192,126-152-cm-v4389/rezolutie-f266,ultra-hd-4k-v21497/tehnologie-display-f3954,oled-v9888/sort-priceasc/c"
url_ipads = base_url + "/tablete/brand/apple/sort-priceasc/c"
url_apple_tv = base_url + "/mediaplayere/brand/apple/sort-priceasc/c"
url_xbox_one_slim = base_url + "/consola-microsoft-xbox-one-slim-500-gb-white-zq9-00010/pd/D01LQ7BBM/"
url_xbox_one_x = base_url + "/consola-microsoft-xbox-one-x-1tb-negru-xb1x/pd/DHVK5KBBM/"
departments_url = base_url + "/all-departments"

seleniumLogic(driver, departments_url, no_of_pages, log_level)

