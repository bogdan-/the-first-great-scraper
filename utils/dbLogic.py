import peewee

database = peewee.SqliteDatabase("wee.db")

# database = peewee.MySQLDatabase("preturiadadb", host="104.238.167.204", user="root", passwd="N3#-password")
#
# class MySQLModel(peewee.Model):
#     """A base model that will use our MySQL database"""
#     class Meta:
#         database = database

class Category(peewee.Model):
    """
    ORM model of the Category table
    """
    try:
        name = peewee.CharField(unique=True)
        url = peewee.CharField(unique=True)
    except Exception as e:
        print("db exception is: ", e)

    class Meta:
        database = database


class Product(peewee.Model):
    """
    ORM model of Product table
    """
    category = peewee.ForeignKeyField(Category)
    title = peewee.CharField(unique=True)
    date_added = peewee.DateTimeField()
    price = peewee.FloatField()

    class Meta:
        database = database


class Price(peewee.Model):
    """
    ORM model of the Category table
    """
    try:
        product = peewee.ForeignKeyField(Product)
        date_added = peewee.DateTimeField()
        value = peewee.FloatField()

    except Exception as e:
        print("db exception is: ", e)

    class Meta:
        database = database


class Link(peewee.Model):
    """
    ORM model of the Link table
    """
    try:
        product = peewee.ForeignKeyField(Product)
        link = peewee.CharField(unique=True)

    except Exception as e:
        print("db exception is: ", e)

    class Meta:
        database = database


class Discount(peewee.Model):
    try:
        product = peewee.ForeignKeyField(Product)
        price = peewee.FloatField()
        discount = peewee.FloatField()
        is_negative = peewee.BooleanField()
        date_added = peewee.DateTimeField()

    except Exception as e:
        print("db exception is: ", e)

    class Meta:
            database = database

class Rating(peewee.Model):
    try:
        product = peewee.ForeignKeyField(Product)
        stars = peewee.FloatField()
        reviews = peewee.FloatField()
        is_negative = peewee.BooleanField()
        date_last_review = peewee.DateTimeField()

    except Exception as e:
        print("db exception is: ", e)

    class Meta:
            database = database

if __name__ == "__main__":
    # database.connect()
    try:
        Category.create_table()
    except peewee.OperationalError:
        print "Category table already exists!"
    except Exception as e:
        print "Category table creation failed!", e

    try:
        Product.create_table()
    except peewee.OperationalError:
        print "Product table already exists!"
    except Exception:
        print "Product table creation failed!"

    try:
        Price.create_table()
    except peewee.OperationalError:
        print "Price table already exists!"
    except Exception:
        print "Price table creation failed!"

    try:
        Link.create_table()
    except peewee.OperationalError:
        print "Link table already exists!"
    except Exception:
        print "Link table creation failed!"

    try:
        Discount.create_table()
    except peewee.OperationalError:
        print "Discount table already exists!"
    except Exception:
        print "Discount table creation failed!"

    try:
        Rating.create_table()
    except peewee.OperationalError:
        print "Rating table already exists!"
    except Exception:
        print "Rating table creation failed!"