def popupBlocker(driver):
    print("&starting popup blocker")
    try:
        bebe = driver.find_element_by_class_name("padiPopupContent")
        if bebe.is_displayed():
            print("# bebe is here !!!")
            close = driver.find_element_by_class_name("padiClose")
            close.click()
    except Exception as e:
        print("# bebe is not here")

    try:
        iframe = driver.find_element_by_class_name("emg-page-wrapper emg-box-sizing emg-department-content")
        if iframe.is_displayed():
            print("# pop-up iframe is here !!!")
    except Exception as e:
        print("# pop-up iframe is not here")

def flashDeals(driver):
    #document.getElementById("flash_deals")
    print("$checking for flash Deals")
    try:
        flashDeal = driver.find_element_by_id("flash_deals")
    except Exception:
        return
    if flashDeal:
        try:
            flashDealObj = driver.find_elements_by_class_name("view-offer-flash-deals")
        except:
            return
        flashDealUrl = flashDealObj.get_attribute("href")
        return flashDealUrl


