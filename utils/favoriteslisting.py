from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


driver = webdriver.Chrome()
base_url = "https://www.emag.ro"
homepage = "/homepage"
favorites = "/favorites"
login = "/user/login"

driver.get(base_url + login)

email = driver.find_element_by_id('email')
email.send_keys('bogdantamirjean@gmail.com')

try:
    while driver.current_url != "https://www.emag.ro/user/account":
        print(driver.current_url)
        WebDriverWait(driver,10)
except Exception as e:
    print("current page not homepage: ", e)
    driver.quit()

driver.get(base_url + favorites)
favourites_list = []
products_list = []
products_list_names = []

# favourites_list = driver.find_elements_by_class_name("gui-stacked-row -hover -wishlist-list")
favourites_list = driver.find_elements_by_css_selector(".gui-stacked-row.-hover.-wishlist-list")
print('len(favourites_list: )',len(favourites_list))
favourites_row_list = driver.find_elements_by_class_name("row-wishlist-name")
if len(favourites_row_list) == len(favourites_list):
    for i in range(0, len(favourites_row_list)):
    # for favorite_sublist in favourites_row_list:
        favourites_row_list[i].click()
        try:
            products_list = driver.find_elements_by_class('row')
        except Exception as e:
            print("products list cannot be found: ", e)
        print('len(products_list: ',len(products_list))
        for product in products_list:
            products_list_names.append(product.text)
        print('products_list_names: ', products_list_names)
        driver.execute_script("window.history.go(-1)")
        favourites_row_list = driver.find_elements_by_class_name("row-wishlist-name")