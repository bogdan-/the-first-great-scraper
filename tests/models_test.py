import unittest
import peewee
from models import Category, Product, Price, Link

# Set up an in-memory SQLite database for testing
test_db = peewee.SqliteDatabase(":memory:")

class TestDatabaseModels(unittest.TestCase):
    def setUp(self):
        # Connect to the test database
        test_db.connect()

        # Create tables for testing
        Category._meta.database = test_db
        Product._meta.database = test_db
        Price._meta.database = test_db
        Link._meta.database = test_db

        Category.create_table()
        Product.create_table()
        Price.create_table()
        Link.create_table()

    def tearDown(self):
        # Close the test database connection
        test_db.close()

    def test_create_category(self):
        # Test creating a Category instance
        category_name = "Electronics"
        category = Category.create(name=category_name)
        self.assertEqual(category.name, category_name)

    def test_create_product(self):
        # Test creating a Product instance
        category = Category.create(name="Clothing")
        product_title = "Summer Dress"
        product_price = 49.99
        product = Product.create(category=category, title=product_title, price=product_price)
        self.assertEqual(product.title, product_title)
        self.assertEqual(product.price, product_price)

    # Add more test methods for other models (Price, Link) as needed

if __name__ == "__main__":
    unittest.main()

