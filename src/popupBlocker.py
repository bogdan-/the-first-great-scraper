def popupBlocker(driver):
    print "&starting popup blocker"
    try:
        bebe = driver.find_element_by_class_name("padiPopupContent")
        if bebe.is_displayed():
            print "# bebe is here !!!"
            close = driver.find_element_by_class_name("padiClose")
            close.click()
    except Exception as e:
        print "# bebe is not here"

    try:
        iframe = driver.find_element_by_class_name("emg-page-wrapper emg-box-sizing emg-department-content")
        if iframe.is_displayed():
            print "# iframe is here !!!"
    except Exception as e:
        print "# iframe is not here"