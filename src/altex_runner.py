import requests
from selenium import webdriver

from src.altex import seleniumLogic

driver = webdriver.PhantomJS()
driver.set_window_size(1280,1080)

base_url = "https://altex.ro"
try:
    r = requests.get(base_url)
except Exception as e:
    print("!!! runner exception at line 27: ", e)
seleniumLogic(driver, base_url)
