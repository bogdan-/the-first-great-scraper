import operator
from bs4 import BeautifulSoup
import logging
import unicodedata
import urllib2
from urllib2 import URLError
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
import traceback
import re
import smtplib
import sys
import time

desirables = ["laptopuri", "telefoanemobile", "cardurimemorie", "tablete", "smartwatch", "desktoppc", "monitoareled",
              "solidstatedrivessd", "routerewireless", "televizoare", "castiaudio", "combinefrigorifice", "frigidere",
              "masinispalatrufe", "pliteincorporabile", "cuptoareincorporabile", "masinidespalatvasestandard",
              "aragauri", "masinidespalatvaseincorporabile", "blenderetocatoare", "fiarestatiiaparatedecalcat",
              "cafetiere", "espressor", "aspiratoare", "aparateaerconditionat", "anvelopeauto"]

def loggingLogic(filename, log_msg):
    try:
        exportFile = open(filename, "a")
        exportFile.write(log_msg)
        exportFile.close()
    except:
        logging.error("@@@EXC@29: logging", sys_info=True)

def emailLogic(msg, to_list):
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.starttls()
    server.login("preturiada@gmail.com", "Liteon123")

    server.sendmail("preturiada@gmail.com", to_list, msg)
    print("# email sent successssfull")
    server.quit()


def cheapestLogic(products_list, discount_list):
    dict = {}
    cheapest_prod_by_category = []
    for i, product in enumerate(products_list):
        try:
            if product[0].category == products_list[i + 1][0].category:
                dict[product[0].category].append(product[0])
                dict[product[0].category] = sorted(dict[product[0].category], key=lambda elems: elems[1])

        except Exception as e:
            continue
    for category in dict:
        cheapest_prod_by_category.append(category[len(products_list) - 1])


def notifyLogic(existent_product, current_price, message):
    addr_list = ["bogdan.tam@me.com", "daneaiulian@yahoo.com"]
    loggingLogic("preturiada.log", message.encode("utf8"))
    try:
        msg = message.encode("utf8")
        emailLogic(msg, addr_list)
    except Exception as e:
        ex_type, ex, tb = sys.exc_info()
        traceback.print_tb(tb)
        print("# email sender failed", e)


def fileCreatorLogic(page_url, driver, log_level):
    delay = 0.1  # seconds
    rating_counter = 0
    print("@ FILE logic starts here ", page_url)
    department_name_from_url = ''.join(e for e in page_url.split("/")[3] if e.isalnum())
    if department_name_from_url in desirables:
        try:
            if log_level == "DEBUG":
                print("DBG: driver oppening the page: ", page_url)
            time.sleep(1)
            driver.get(page_url)
        except Exception:
            logging.error("@@@exc@80 1st opening crash", exc_info = True)
            try:
                driver.refresh()
                logging.error("@@@exc@81 1st refresh crash", exc_info=True)
            except Exception:
                try:
                    driver.get(page_url)
                except URLError:
                    print("@@@exc@86: 2nd open got URLError")
                    time.sleep(5)
                    try:
                        driver.get(page_url)
                    except Exception:
                        try:
                            driver.refresh()
                        except Exception:
                            logging.error(
                                "@@@exc@94: 2nd refreshing did not work: ",
                                exc_info = True
                            )
                            return

                except Exception:
                    logging.error("@@@exc@98: 2nd opening crash: ", exc_info=True)
                    try:
                        time.sleep(5)
                        driver.get(page_url)
                    except Exception:
                        try:
                            driver.refresh()
                        except Exception:
                            logging.error(
                                "@@@exc@106: 3rd refreshing did not work: ",
                                exc_info=True
                            )
                            return
    else:
        try:
            if log_level == "DEBUG":
                print("DBG: driver oppening the page: ", page_url)
            time.sleep(1)
            driver.get(page_url)
        except Exception:
            logging.error(
                "@@@exc@121: undesirable category page opening did not work",
                exc_info=True
            )
            return


    if log_level == "DEBUG":
        print("DBG: checking url for redirection")
    if page_url != driver.current_url:
        print("@@@131@1 current URL different than the page desired {} vs {}".format(
            page_url,
            driver.current_url
        ))
        time.sleep(5)
        try:
            if log_level == "DEBUG":
                print("@@@138@1DBG: driver oppening the page: {}".format(page_url))
                driver.get(page_url)
        except Exception:
            logging.error("@@@exc@141 oppening the differetn page", exc_info=True)
            return "different_url"
        if page_url != driver.current_url:
            print("@@@144@2 current URL different than the page desired", page_url, driver.current_url)
            return "different_url"
    print("@146@ page LOADED")
    pagesNoElem = 1
    pricesList = []
    prodDict = {}
    pricesNo = 0
    ratingsNo = 0

    try:
        #  print("@ getting the list of cards")
        lists = driver.find_elements_by_class_name("card-section-wrapper")
    except Exception:
        logging.error("@@@exc@155: couldnt find card-section-wrapper", exc_info=True)
        return

    # get the number of elements found
    # print("@ Found " + str(len(lists)) + " cards:")

    # iterate through each element and print the text that is
    print("@ parsing the list: ", len(lists))
    for listitem in lists:
        try:
            price = listitem.find_element_by_class_name("product-new-price")
            pricesNo += 1
        except Exception as e:
            print("@Exception at line 169: ", e)
            return
        if rating_counter < 3:
            try:
                no_reviews = WebDriverWait(listitem, delay).until(
                    EC.presence_of_element_located((By.CLASS_NAME, 'star-rating-text')))
                try:
                    no_reviews = int(re.findall("\d+", no_reviews.text)[0])
                except Exception as e:
                    print("@Exc@178: didn't find reviews", e)
                try:
                    rating_elem = WebDriverWait(listitem, delay).until(
                        EC.presence_of_element_located((By.CLASS_NAME, 'star-rating-inner')))
                    rating = rating_elem.get_attribute("style")
                    if rating:
                        try:
                            rating = float(re.findall("\d+\.\d+", rating)[0])
                        except:
                            rating = float(re.findall("\d+", rating)[0])
                        ratingsNo += 1
                except TimeoutException:
                    rating = 0
                    print("@Exc@192: didn't find rating")

            except TimeoutException:
                print("@Exc@195: didn't find no_reviews")
                rating_counter+=1
                no_reviews=0
                rating = 0
        else:
            no_reviews = 0
            rating = 0
        try:
            product = listitem.find_element_by_class_name("product-title-zone")

            html = product.get_attribute('outerHTML')
            attrs = BeautifulSoup(html, 'html.parser').a.attrs
            url = unicodedata.normalize('NFKD', attrs["href"]).encode('ascii', 'ignore')

            name = product.text
            if log_level == "ERROR":
                print(["name is:", name[0:10]])
        except Exception as e:
            print("@Exception @ line 213:", e)
            return

        try:
            stringPriceWithCurr = price.text
            if log_level == "DEBUG" or log_level == "ERROR":
                time.sleep(0.1)
            if stringPriceWithCurr.startswith("de"):
                try:
                    floatPrice = float(stringPriceWithCurr.split(" ")[2])
                except ValueError:
                    print("@ Exception @ line 224: Not a float")
                    continue
            else:
                try:
                    floatPrice = float(stringPriceWithCurr.split(" ")[0])
                except ValueError:
                    print("@ Exception @ line 230: Not a float")
                    continue

            if floatPrice < 20:
                floatPrice = round(floatPrice * 1000, 3)
            else:
                floatPrice = floatPrice / 100
            if floatPrice < 1:
                floatPrice = round(floatPrice * 100000, 3)
            if floatPrice > 10000000:
                floatPrice = floatPrice / 100000

        except Exception as e:
            print("@ Exception @ line 243: ", e)
            continue
        pricesList.append(floatPrice)
        prodDict[name] = [floatPrice, url, rating, no_reviews]
    if log_level == 'DEBUG':
        print(pricesNo, ratingsNo, " prices and ratings found for this no of products: ", len(lists))
    return prodDict

def flashCreatorLogic(campaign_url, log_level):
    print("@ FLASH logic starts here ", campaign_url)
    pricesList = []
    prodDict = {}
    if campaign_url != None:
        #load the page:
        try:
            if log_level == "DEBUG":
                print("DBG: driver oppening the FLASH deals page: ", campaign_url)
            page = urllib2.urlopen(campaign_url)
            pageHtml = page.read()
            pageSoup = BeautifulSoup(pageHtml, 'html.parser')
        except Exception as e:
            print("@@@exc@262: ", e)
            return
        if page.url == 'https://www.emag.ro/':
            return
        articles = pageSoup.find_all('article')
        if articles:
            for article in articles:
                if log_level == "DEBUG":
                    print("@fl270 found price element")
                try:
                    for articleName in article.h2.stripped_strings:
                        name = articleName
                except Exception as e:
                    print("@Exp@275: ", e)
                if name:
                    try:
                        name = name.encode('ascii', 'ignore')
                    except Exception as e:
                        print(['@Exp@280: ', name, e])
                else:
                    return prodDict #if name fails to exist try to return the dict of products created so far
                try:
                    urlRaw = article.find("a")['href']
                    url = 'https://www.emag.ro'+str(urlRaw.split("/?")[0])+'/'
                except Exception as e:
                    print(['@Exp@284: ', article, e])
                priceInt = article.select(".spi-current-price")[0].select(".money-int")[0].text
                priceDecimal = None
                try:
                    priceDecimal = article.select(".spi-current-price")[0].select(".money-decimal")[0].text
                except Exception as e:
                    print("@Exp@293: ", e)
                    # return go on
                if priceDecimal:
                    price = priceInt + priceDecimal.replace(',', '.')
                else:
                    price = priceInt
                price = price.encode('ascii', 'ignore')

                if log_level == "ERROR":
                    print(["name is:", name[0:10]])
                try:
                    floatPrice = float(price)
                except ValueError:
                    print("@Exc@306: Not a float")
                    continue
                try:
                    pricesList.append(floatPrice)
                    prodDict[name] = [floatPrice, url]
                except Exception as e:
                    print("@Exc@312 ", e)
                    continue
        else:
            products = pageSoup.find_all('div', {'class': ['product-card']})
            for product_html in products:
                try:
                    name = product_html.h2.string.encode('ascii', 'ignore')

                    urlRaw = product_html.find("a")['href']
                    url = 'https://www.emag.ro' + str(urlRaw.split("/?")[0]) + '/'
                    pretRaw = product_html.find('div', {'class': 'product-price-current'})
                    floatPrice = float(pretRaw.contents[0].replace(".","")) + \
                           float(pretRaw.sup.string)*float(10**((-1)*len(pretRaw.sup.string)))
                    pricesList.append(floatPrice)
                    prodDict[name] = [floatPrice, url]
                except Exception as e:
                    print("@Exc@309 ", e)
                    continue


        print("@@@FLd@got the prodDict len: ", len(prodDict))
        return prodDict