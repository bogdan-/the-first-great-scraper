import traceback
import math

from src.creatorpagetwo import *


def seleniumLogic(driver, url):

    start_time = datetime.datetime.now()
    app_url = "http://www.preturi.date/index.php?r=product%2F"
    base_url = "https://altex.ro"
    namesForFashion = [
                       ]
    unwantedCategoriesList = []
    desiredList = [1, 293, 60, 65, 147
                   ]
    print("# Selenium logic starts here")
    # create a new Firefox session
    print("# creating the driver")
    # navigate to the application home page
    driver.get(url)
    urlList = []  # list of "/department/" urls
    subUrlList = []
    tableNameList = []
    departsWebElemListe = []  # list of webdriver.remote.webelement.WebElement elements
    cheap_products = []
    discount_of = {}

    # get the list of department elements
    try:
        print("# getting the list of elements")
        departmentsPage = driver.find_elements_by_class_name(
            "ProductsMenu-item")
        for i, dep in enumerate(departmentsPage):
            html = dep.get_attribute('outerHTML')
            attrs = BeautifulSoup(html, 'html.parser').a.attrs
            url = attrs["href"]
            urlList.append(url)
            if i>2:
                break
        for url in urlList:
            try:
                driver.get(str(url))
            except Exception as e:
                print("# BROKE 'cause Exception at line 44: ",e)
                break
            subdepts = driver.find_elements_by_class_name(
            "Products-item")
            for dep in subdepts:
                html = dep.get_attribute('outerHTML')
                attrs = BeautifulSoup(html, 'html.parser').a.attrs
                url = attrs["href"]
                subUrlList.append(str(url))


    except Exception as e:
        print("# Exception at line 56", e)
        ex_type, ex, tb = sys.exc_info()
        traceback.print_tb(tb)

    for i, subdeptUrl in enumerate(subUrlList):
        print("# getting the list of products")
        try:
            driver.get(subdeptUrl)
        except Exception as e:
            print("# BROKE 'cause Exception at line 66: ", e)
            driver.quit()
            break

        try:
            filters = driver.find_elements_by_class_name("filter-option")
        except Exception as e:
            continue
        for filter_name in filters:
            is_stocfurnizor = False
            filter_name_html =  filter_name.get_attribute('outerHTML')
            if 'In stoc (' in BeautifulSoup(filter_name_html,"html.parser").a.string:
                products_no = int(BeautifulSoup(filter_name_html,"html.parser").a.string.split("(",1)[1][:-1])
                subdeptUrl = BeautifulSoup(filter_name_html,"html.parser").a['href']

            if 'In stoc furnizor' in BeautifulSoup(filter_name_html,"html.parser").a.string:
                is_stocfurnizor = True
                provider_products_no = int(BeautifulSoup(filter_name_html,"html.parser").a.string.split("(",1)[1][:-1])
                provider_subdeptUrl = BeautifulSoup(filter_name_html, "html.parser").a['href']
        for j in range(1, int(math.ceil(float(products_no)/48))):
            if j==1:
                subdept_newUrl = subdeptUrl
            else:
                subdept_newUrl = subdeptUrl + 'p/' + str(j)

            try:
                driver.get(subdept_newUrl)
            except Exception as e:
                print("# BROKE 'cause Exception at line 96: ", e)
                driver.quit()
            print("# url: ", subdept_newUrl)
            try:
                products = driver.find_elements_by_class_name(
                    "Product")
            except Exception as e:
                print("#Exception at line 103: ", e)
            for produs in products:
                html_product = produs.get_attribute('outerHTML')
                attrs = BeautifulSoup(html_product,"html.parser")
                try:
                    name = attrs.a['title']
                except Exception as e:
                    print("# Exception at line 112: ", html_product)
                # pret = driver.find_element_by_class_name(
                #     'Price-current')
                html_product_price = produs.get_attribute('outerHTML')
                attrs_product_price = BeautifulSoup(html_product_price,"html.parser")
                pret = float((attrs_product_price.span.string + attrs_product_price.sup.string).replace(".", "").replace(",","."))
                print("pret, name: ", pret, name)

            if is_stocfurnizor == True:
                for j in range(1, int(math.ceil(float(provider_products_no) / 48))):
                    if j == 1:
                        subdept_newUrl = provider_subdeptUrl
                    else:
                        subdept_newUrl = provider_subdeptUrl + 'p/' + str(j)

                    try:
                        driver.get(subdept_newUrl)
                    except Exception as e:
                        print("# BROKE 'cause Exception at line 96: ", e)
                        driver.quit()
                    print("# url: ", subdept_newUrl)
                    try:
                        products = driver.find_elements_by_class_name(
                            "Product")
                    except Exception as e:
                        print("#Exception at line 103: ", e)
                    for produs in products:
                        html_product = produs.get_attribute('outerHTML')
                        attrs = BeautifulSoup(html_product, "html.parser")
                        name = attrs.a['title']

                        # pret = driver.find_element_by_class_name(
                        #     'Price-current')
                        html_product_price = produs.get_attribute('outerHTML')
                        attrs_product_price = BeautifulSoup(html_product_price, "html.parser")
                        pret = float(
                            (attrs_product_price.span.string + attrs_product_price.sup.string).replace(".", "").replace(
                                ",", "."))
                        print("pret, name: ", pret, name)

#
# ############################################################### $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
#     # for j, departmentName in enumerate(tableNameList[975:], start=975):
#     # for j, departmentName in reversed(list(enumerate(tableNameList))):
#     for j, departmentName in enumerate(tableNameList):
#         print("j = ", j)
#         if j > len(urlList)-1:
#             continue
#         else:
#             if [n for n in namesForFashion if n in departmentName or n in urlList[j]]:  #list comprehension made by me
#                 print("#92 List comprehention")
#                 continue
#             else:
#                 print("##2 department name is: ", departmentName, urlList[j], tableNameList[j])
#                 print("# fileCreator Logic")
#                 popupBlocker(driver)
#                 for x in range(1,70):
#                     if x == 1:
#                         link_url = urlList[j] + 'c'
#                     else:
#                         link_url = urlList[j] + 'p' + str(x) + '/c'  # if multiple pages needed to follow add: + "p2/c" ...
#                     products = fileCreatorLogic(base_url + link_url, driver)  # here you add the url page if you need a specific one
#
#                     if products is None:
#                         break  # break here if fileCreatorLogic fails to return something in 'products' attr
#                     else:
#                         for k, produs in enumerate(products.keys()):
#                             is_negative = False
#                             if k == 1:
#                                 print("# DB Logic")
#                             if k == 50:
#                                 print("# DB Logic 4 50th product")
#                             pret = float(products[produs][0])
#                             link = products[produs][1]
#
#                             # exportFile = open(departmentName, "w")
#                             # exportFile.write(str(products))
#                             # exportFile.close()
#                             ######################################
#                             try:
#
#                                 try:
#                                     existent_category = Category.select().where(Category.name == departmentName).get()
#                                 except Category.DoesNotExist:
#                                     existent_category = None
#
#                                 if existent_category is None:
#                                     existent_category = Category.create(name=departmentName)
#
#                                 try:
#                                     existent_product = Product.select().where(Product.title == produs).get()
#                                 except Product.DoesNotExist:
#                                     # print("### Exception at line 140, product is not here: ", produs)
#                                     existent_product = None
#
#                                 if existent_product is None:
#                                     product_one = Product(category=existent_category,
#                                                           title=produs,
#                                                           date_added=datetime.datetime.now(),
#                                                           price=pret)
#                                     last_price = Price(product=product_one,
#                                                        date_added=datetime.datetime.now(),
#                                                        value=pret)
#                                     link_url = Link(product = product_one,
#                                                     link = link)
#                                     product_one.save()
#                                     last_price.save()
#                                     try:
#                                         link_url.save()
#                                     except Exception as e:
#                                         print("# exception at link saving, line 158 ",link, e, "\n TRACEBACK is: ")
#                                         ex_type, ex, tb = sys.exc_info()
#                                         traceback.print_tb(tb)
#                                         screenshot_title = str(datetime.datetime.now()) + '#' + str(produs.encode("utf8")+".png")
#                                         driver.save_screenshot(screenshot_title)
#                                         continue
#
#                                 else:
#                                     last_price = Price(product=existent_product,
#                                                        date_added=datetime.datetime.now(),
#                                                        value=pret)
#
#                                     if pret != existent_product.price:
#                                         log = u"\n{} the product with ID_type {} and ID {}, from {} category, has a different price: \n{} coming from {}".format(
#                                             datetime.datetime.now(), type(existent_product.id), existent_product.id, existent_category.name, pret, existent_product.price
#                                         )
#                                         loggingLogic("preturiada.log", log.encode("utf8"))
#                                         discount = round(pret - existent_product.price, 3)
#                                         if discount < 0:
#                                             is_negative = True
#
#                                         if existent_product.category_id in desiredList and pret < existent_product.price*0.75:
#                                             cheaper_by = existent_product.price - pret
#                                             url_of_cheaper_product = app_url + "view&id=" + str(existent_product.id)
#                                             msg = u"\n {} has a new price: \n {} \n coming down from: \n {} being cheaper by {}" \
#                                                   u"\n More details here: {}".format(
#                                                 existent_product.title, pret, existent_product.price, cheaper_by,
#                                                 url_of_cheaper_product)
#                                             notifyLogic(existent_product, pret, msg)
#                                         # if pret < existent_product.price*0.75:
#                                         #     cheap_products.append([existent_product, round(pret - existent_product.price, 3)])
#                                         #     discount_of[existent_product] = round(pret - existent_product.price, 3)
#
#
#                                         prod_discount = Discount(product = existent_product,
#                                                                  price = pret,
#                                                                  discount = discount,
#                                                                  is_negative = is_negative
#                                         )
#                                         prod_discount.save()
#
#                                         existent_product.price = pret
#                                         last_price.save()
#
#                                     existent_product.category = existent_category
#                                     existent_product.save()
#                             except Exception as e:
#                                 print(j, "# exception at line 201 ", e, "\n TRACEBACK is: ")
#                                 ex_type, ex, tb = sys.exc_info()
#                                 traceback.print_tb(tb)
#                                 # screenshot_title = str(datetime.datetime.now())+'#'+str(produs.encode("utf8")+".png")
#                                 # print(screenshot_title)
#                                 # driver.save_screenshot(screenshot_title)
#                                 continue
#     # cheapestLogic(cheap_products, discount_of)
#     try:
#         run_time = datetime.datetime.now() - start_time
#         final_msg = u"\n YOUR script has ended in {} \n for these links:\n {}".format(
#             str(divmod(run_time.days * 86400 + run_time.seconds, 60)), urlList)
#         emailLogic(final_msg.encode("utf8"),['bogdan.tam@me.com'])
#     except Exception as e:
#         print("# email sender fail", e)
#         driver.quit()
