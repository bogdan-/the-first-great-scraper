import operator
from bs4 import BeautifulSoup
import unicodedata
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import traceback
import smtplib
import sys
import os
import paramiko
import datetime

def loggingLogic(filename, log_msg):
    exportFile = open(filename, "a")
    exportFile.write(log_msg)
    exportFile.close()

def emailLogic(msg, to_list):

    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.starttls()
    server.login("preturiada@gmail.com", "Liteon123")

    server.sendmail("preturiada@gmail.com", to_list, msg)
    print("# email sent successssfull")
    server.quit()

def scpLogic(server,username,password):
    '''
    ssh = paramiko.SSHClient()
    ssh.load_host_keys(os.path.expanduser(os.path.join('~','envs','emag','emagenv','wee.db')))
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(server, username=username, password=password)
    sftp = ssh.open_sftp()
    sftp.put("/Users/bogdan/envs/emag/emagenv/wee.db","/var/www/preturi.data/db/wee.db")

    
    t = paramiko.Transport((hostname, port))
    t.connect(username=username, password=password, hostkey=hostkey)
    sftp = paramiko.SFTPClient.from_transport(t)
    sftp.put('test.zip', '/tmp/test.zip')
    '''
def cheapestLogic(products_list, discount_list):
    dict={}
    cheapest_prod_by_category=[]
    for i, product in enumerate(products_list):
        try:
            if product[0].category == products_list[i+1][0].category:
                dict[product[0].category].append(product[0])
                dict[product[0].category] = sorted(dict[product[0].category], key = lambda elems: elems[1])

        except Exception as e:
            continue
    for category in dict:
        cheapest_prod_by_category.append(category[len(products_list) - 1])




def notifyLogic(existent_product, current_price, message):
    addr_list = ["bogdan.tam@me.com", "daneaiulian@yahoo.com"]
    loggingLogic("preturiada.log", message.encode("utf8"))
    try:
        msg = message.encode("utf8")
        emailLogic(msg, addr_list)
    except Exception as e:
        ex_type, ex, tb = sys.exc_info()
        traceback.print_tb(tb)
        print("# email sender fail", e)


def fileCreatorLogic(page_url, driver, log_level):
    print("@ FILE logic starts here ", page_url)
    try:
        if log_level=="DEBUG":
            print("DBG: driver oppening the page: ", page_url)
        driver.get(page_url)
    except Exception as e:
        print("@@@exc@80: ", e)
        try:
            driver.refresh()
        except Exception:
            try:
                driver.get(page_url)
            except Exception as e:
                print("@@@exc@85: refreshing did not work: ", e)
                return
    if log_level == "DEBUG":
        print("DBG: checking url for redirection")
    if page_url != driver.current_url:
        print("@@@ current URL different than the page desired", page_url, driver.current_url)
        return
    print("@ page LOADED")
    pagesNoElem = 1

    # print("@ getting number of pages")
    # try:
    #     try:
    #         element = WebDriverWait(driver,5).until(
    #         EC.visibility_of_element_located((By.CLASS_NAME, 'js-report-product-count')))
    #     except Exception as e:
    #         print("@ Exception @ line 21: Trying a refresh", e)
    #         driver.refresh()
    #         element = WebDriverWait(driver, 15).until(
    #             EC.visibility_of_element_located((By.CLASS_NAME, 'js-report-product-count')))
    # except Exception as e:
    #     print("@ Exception @ line 97", e, traceback.format_exc())
    #     pagesNoElem = 1
    #
    # if element is not None:
    #     try:
    #         pagesNoText = driver.find_element_by_class_name("js-report-product-count").text
    #         pagesNoElem = pagesNoText.encode("utf8", "ignore")
    #     except Exception as e:
    #         print("@ Exception @ line 105", e, traceback.format_exc())
    #         return
    #
    # print("@ pagesNo: ", pagesNoElem)
    # pagesNo = int(filter(str.isdigit, pagesNoElem))/60
    # print(pagesNo)




    # paginatedURL = element.split("/c")[0] + "/p" + str(i) + "/c"
    try:
        #  print("@ getting the list of cards")
        lists = driver.find_elements_by_class_name("card-section-wrapper")
    except Exception as e:
        print(e)
        return

    # get the number of elements found
    # print("@ Found " + str(len(lists)) + " cards:")

    # iterate through each element and print the text that is
    pricesList = []
    prodDict = {}
    print("@ parsing the list: ", len(lists))
    for listitem in lists:
        try:
            price = listitem.find_element_by_class_name("product-new-price")
            if log_level == "DEBUG":
                print("found price element")
        except Exception as e:
            print("@Exception at line 100: ", e)
            return
        product = listitem.find_element_by_class_name("product-title-zone")

        html = product.get_attribute('outerHTML')
        attrs = BeautifulSoup(html, 'html.parser').a.attrs
        url = unicodedata.normalize('NFKD', attrs["href"]).encode('ascii', 'ignore')
        try:
            name = product.text
            if log_level == "ERROR":
                print(["name is:", name[0:10]])
        except Exception as e:
            print("@Exception @ line 144:", e)
            return

        try:
            stringPriceWithCurr = price.text
            if log_level == "DEBUG" or log_level == "ERROR":
                print(["price is: ", stringPriceWithCurr])
            if stringPriceWithCurr.startswith("de"):
                try:
                    floatPrice = float(stringPriceWithCurr.split(" ")[2])
                except ValueError:
                    print("@ Exception @ line 48: Not a float")
                    continue
            else:
                try:
                    floatPrice = float(stringPriceWithCurr.split(" ")[0])
                except ValueError:
                    print("@ Exception @ line 54: Not a float")
                    continue

            if floatPrice < 20:
                floatPrice = round(floatPrice * 1000, 3)
            else:
                floatPrice = floatPrice / 100
            if floatPrice < 1:
                floatPrice = round(floatPrice * 100000, 3)
            if floatPrice > 10000000:
                floatPrice = floatPrice / 100000

        except Exception as e:
            print("@ Exception @ line 65: ", e)
            continue
        pricesList.append(floatPrice)
        prodDict[name]=[floatPrice, url]
    return prodDict
    # else:
    #     return
        # lists = []
        # try:
        #     print "@ getting the list of cards"
        #     lists = driver.find_elements_by_class_name("card-section-wrapper")
        # except Exception as e:
        #     print("@@@ Exceptie @ line 76: ", e)
        #     return
        #
        # # get the number of elements found
        #
        # # iterate through each element and print the text that is
        # pricesList = []
        # prodDict = {}
        # print "@ parsing the list"
        #
        # for i, listitem in enumerate(lists):
        #     price = listitem.find_element_by_class_name("product-new-price")
        #     product = listitem.find_element_by_class_name("product-title-zone")
        #     html = product.get_attribute('outerHTML')
        #     attrs = BeautifulSoup(html, 'html.parser').a.attrs
        #     url = unicodedata.normalize('NFKD', attrs["href"]).encode('ascii', 'ignore')
        #
        #     name = product.text
        #
        #     try:
        #         stringPriceWithCurr = price.text
        #         if stringPriceWithCurr.startswith("de"):
        #             try:
        #                 floatPrice = float(stringPriceWithCurr.split(" ")[2])
        #             except ValueError:
        #                 print ("@ Exception @ line 100: Not a float")
        #                 continue
        #         else:
        #             try:
        #                 floatPrice = float(stringPriceWithCurr.split(" ")[0])
        #             except ValueError:
        #                 print ("@ Exception @ line 106: Not a float")
        #                 continue
        #         print "@ price adjustments"
        #         if floatPrice < 20:
        #             floatPrice = round(floatPrice * 1000, 3)
        #         else:
        #             floatPrice = floatPrice / 100
        #         if floatPrice < 1:
        #             floatPrice = round(floatPrice * 100000, 3)
        #
        #         big_ass_price = str(floatPrice)
        #         if len(big_ass_price) > 7:
        #             floatPrice = round(floatPrice * 100000, 3)
        #         if floatPrice > 10000000:
        #             floatPrice = floatPrice / 100000
        #     except Exception as e:
        #         print("@ Exception @ line 117: ", e)
        #         continue
        #     pricesList.append(floatPrice)
        #     prodDict[name] = [floatPrice, url]
        # return prodDict

