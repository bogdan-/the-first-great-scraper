import datetime
import traceback

from src.creator import *
from utils.dbLogic import *
from utils.popupBlocker import popupBlocker

desirables = ["laptopuri", "telefoanemobile", "cardurimemorie", "tablete", "smartwatch", "desktoppc", "monitoareled",
              "solidstatedrivessd", "routerewireless", "televizoare", "castiaudio", "boxeportabile", "combinefrigorifice", "frigidere",
              "masinispalatrufe", "pliteincorporabile", "cuptoareincorporabile", "masinidespalatvasestandard",
              "aragauri", "masinidespalatvaseincorporabile", "blenderetocatoare", "fiarestatiiaparatedecalcat",
              "cafetiere", "espressor", "aspiratoare", "aparateaerconditionat", "anvelopeauto"]
desirables_max_page_depth = 550
def flashDealsLogic(url):
    today = datetime.datetime.today()
    list_of_months = ['','ian','feb','mar','apr','mai','iun','iul','aug','sep','oct','noi','dec']
    url = url + '/cmp/flash-deals-{}-{}-{}'.format(today.day,list_of_months[today.month],today.year)

    '''
    print("# flash dealse logic starts here with DRIVER navigation to", url)
    campUrl = None
    # navigate to the application home page

    try:
        page = urllib2.urlopen(url)
        pageHtml = page.read()
        pageSoup = BeautifulSoup(pageHtml, 'html.parser')
    except Exception as e:
        print(e)
    try:
        products = pageSoup.find_all('div', {'class': ['product-card']})
    except Exception as e:
        products = None
        print(e)
    if products != None:
        '''
    '''
    try:
        driver.get(url)
    except Exception as e:
        print("exc@33: ", e)
        time.sleep(5)
        try:
            driver.get(url)
        except Exception as e:
            print("exc@38: ", e)
    if str(url) + "/" == driver.current_url:
        try:
            isFlashDeals = driver.find_element_by_class_name("view-offer-flash-deals")
            baseURI = isFlashDeals.get_property('attributes')[0]['nodeValue']
            campUrl = str(url) + str(baseURI)
        except:
            print("# flash dealse logic didn't find a FLASH DEAL")
        print(['# found flash dealse: ', campUrl])
    return campUrl
    '''
    return url


def seleniumLogic(driver, url, max_page_depth, log_level):
    start_time = datetime.datetime.now()

    print([start_time, "and pages to scan: ", max_page_depth])
    app_url = "http://www.preturi.date/index.php?r=product%2F"
    base_url = "https://www.emag.ro"
    namesForFashion = ["imbracaminte", "fashion", "fossil", "Ceasuricopii", "Accesoriiorga",
                       "maternitate", "gravide", "motocultoare", "tonere", "portjartiere",
                       "alimente", "alimente-sanatoase", "alimentesanatoase"
                       ]
    unwantedCategoriesList = ["supermarket"]
    desiredList = [1, 293, 60, 65, 147
                   ]
    print("# Selenium logic starts here with DRIVER navigation to", url)

    # navigate to the application all-departments page
    try:
        driver.get(url)
    except Exception as e:
        print("exc@69: ", e)
        time.sleep(5)
        try:
            driver.get(url)
        except Exception as e:
            print("exc@74: ", e)
            return
    urlList = []  # list of "/department/c" type strings
    tableNameList = []
    departsWebElemListe = []  # list of webdriver.remote.webelement.WebElement elements
    cheap_products = []
    discount_of = {}
    urls_file = open('url_file.txt', 'w')
    depts_file = open('depts_file.txt', 'w')

    # get the list of department elements
    try:
        print("# getting the list of elements")
        departmentsPage = driver.find_elements_by_id(
            "departments-page")
        for dep in departmentsPage:
            departments = dep.find_elements_by_id(
                "department-expanded")
            for depExpanded in departments:
                departmentsExpanded = depExpanded.find_elements_by_tag_name(
                    "li")
                # print("#new# departments Expanded: ", len(departments))
                for s, subdept in enumerate(departmentsExpanded):
                    subdepartment = subdept.find_elements_by_tag_name(
                        "li")
                    for i, el in enumerate(subdepartment):  # here el is a webdriver.remote.webelement.WebElement
                        print(["#...", s, i])

                        departsWebElemListe.append(el)  # list of webdriver.remote.webelement.WebElement elements
                        html = el.get_attribute('outerHTML')
                        attrs = BeautifulSoup(html, 'html.parser').a.attrs
                        url = attrs["href"]
                        urlSplitted = url.split("?")[0][
                                      :-1]  # split the url and then delete the last character (/c) from the url

                        if not [m for m in unwantedCategoriesList if m in urlSplitted]:
                            urlList.append(urlSplitted)
                        else:
                            continue

                    #     if i>5 and s>5:
                    #         break
                    # if s>5:
                    #     break

                    # print(len(urlList), urlSplitted)
        urls_file.write("%s\n" % urlList)
        depts_file.write("%s\n" % departsWebElemListe)
    except Exception as e:
        print("# Exc@124", e)
        ex_type, ex, tb = sys.exc_info()
        traceback.print_tb(tb)
    finally:
        depts_file.close()
        urls_file.close()

    # get the number of elements found
    # print("# departments Expanded: ", len(departmentsListe))

    # iterate through each element and print the text that is
    print("# parsing the list")

    for i, department in enumerate(departsWebElemListe):
        # if i > 100:
        #     break
        if len(department.text) > 80:
            print('!@#$%^&**&^%$#@! ', department.text)
            continue
        else:
            if i == 1 or i == 100 or i == 1000:
                print("# dep names")
                print(i, tableName)
            name = department.text
            tableName = ''.join(e for e in name if e.isalnum())
            if not [m for m in unwantedCategoriesList if m in tableName]:
                tableNameList.append(tableName)
            else:
                continue
    start = 0
    stop = 975

    ############################################################### $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    # for j, departmentName in reversed(list(enumerate(tableNameList))):
    #    for j, departmentName in enumerate(tableNameList[start:stop], start=start):
#while True:
    for j, departmentName in enumerate(tableNameList):
        print(["j = ", j])
        if j > len(urlList) - 1:
            continue
        else:
            if [n for n in namesForFashion if n in departmentName or n in urlList[j]]:  # list comprehension made by me
                print("#103 entered List comprehention for: ", urlList[j])
                continue
            else:
                if log_level == "DEBUG":
                    print("#127 department name is: ", departmentName, urlList[j], tableNameList[j])
                print("# fileCreator Logic")
                popupBlocker(driver)
                if departmentName in desirables:
                    max_page = desirables_max_page_depth
                else:
                    max_page = max_page_depth
                for x in range(1, max_page):
                    if x == 1:
                        link_url = urlList[j] + 'c'
                    else:
                        link_url = urlList[j] + 'p' + str(
                            x) + '/c'  # if multiple pages needed to follow add: + "p2/c" ...
                        campaign = None
                    print("max_page is {}".format(max_page))
                    products = fileCreatorLogic(base_url + link_url, driver,
                                                log_level)  # here you add the url page if you need a specific one
                    category_link = base_url + link_url

                    if products is None:
                        print("#127 EXITING: products was none")
                        f = open("none_products_exited_category.txt", "a+")
                        f.write("{} none_products for this link: {}".format(datetime.datetime.now(), link_url))
                        return  # break here if fileCreatorLogic fails to return something in 'products' attr
                    else:
                        if products == "different_url":
                            break  # break here if fileCreatorLogic fails to return something in 'products' attr
                        else:
                            print(["# no of returned products: ", len(products)])
                            for k, produs in enumerate(products.keys()):
                                is_negative = False
                                rating_is_negative = False
                                pret = float(products[produs][0])
                                link = products[produs][1]
                                rating = products[produs][2]
                                no_reviews = products[produs][3]
                                try:
                                    no_of_stars = rating / 20
                                except Exception:
                                    print("no rating or no no_reviews")
                                if k == 2:
                                    print("### DB Logic for: ", k)
                                if log_level == "DEBUG":
                                    if k % 10 == 0:
                                        print("# DB Logic for {}th product with pret {}, rating {} and link {}".format(k, pret, link, rating))

                                # exportFile = open(departmentName, "w")
                                # exportFile.write(str(products))
                                # exportFile.close()
                                ######################################
                                try:
                                    try: # getting the category by its name
                                        existent_category = Category.select().where(
                                            Category.name == departmentName
                                        ).get()
                                    except Category.DoesNotExist:
                                        existent_category = None

                                    if existent_category is None:
                                        existent_category = Category.create(name=departmentName, url=category_link)

                                    if existent_category.url is None:
                                        existent_category.url = category_link

                                        existent_category.save(only=[Category.url])

                                    try: # getting the product by its title
                                        existent_product = Product.select().where(
                                            Product.title == produs
                                        ).get()
                                        if existent_category != None:
                                            if existent_product.category_id != existent_category.id:
                                                existent_product.category = existent_category

                                    except Product.DoesNotExist:
                                        existent_product = None

                                    try: # getting the link of a product
                                        existent_product_link = Link.select().where(
                                            Link.link == link
                                        ).get()
                                    except Link.DoesNotExist:
                                        existent_product_link = None
                                    if existent_product_link and existent_product is None:
                                        # get the product by its url
                                        existent_product = Product.select().where(
                                            Product.id == existent_product_link.product_id
                                        ).get()

                                    if existent_product is None:
                                        product_one = Product(
                                            category=existent_category,
                                            title=produs,
                                            date_added=datetime.datetime.now(),
                                            price=pret)
                                        last_price = Price(
                                            product=product_one,
                                            date_added=datetime.datetime.now(),
                                            value=pret)
                                        link_url = Link(
                                            product=product_one,
                                            link=link)
                                        rating = Rating(
                                            product=product_one,
                                            stars=no_of_stars,
                                            reviews=no_reviews,
                                            is_negative=False,
                                            date_last_review=datetime.datetime.now()
                                        )
                                        product_one.save()
                                        last_price.save()
                                        rating.save()
                                        try:
                                            link_url.save()
                                        except Exception as e:
                                            print("# exception at link saving, line 273 ", link, e, "\n TRACEBACK is: ")
                                            ex_type, ex, tb = sys.exc_info()
                                            traceback.print_tb(tb)
                                            log_msg = u"{} the product {} found here: {}, exception: {}, traceback: {}\n \n".format(
                                                datetime.datetime.now(), produs, link, e, traceback.print_tb(tb)
                                            )
                                            loggingLogic('logging_for_link_constraint.log', log_msg.encode("utf8"))
                                            print(log_msg.encode("utf8"))
                                            # screenshot_title = str(datetime.datetime.now()) + '#' + str(produs.encode("utf8")+".png")
                                            # driver.save_screenshot(screenshot_title)
                                            continue

                                        if log_level == "DEBUG":
                                            if k % 10 == 0:
                                                try:
                                                    print("#256# produsu e {} cu pret {}, rating {} si url {}".format(product_one.title, last_price.value, rating.stars, link_url.link))
                                                except:
                                                    print("cannot print db values for product: {}".format(produs))

                                    else:
                                        try:
                                            existent_rating = Rating.select().where(
                                                Rating.product_id == existent_product.id
                                            ).get()
                                            print("@@@ existent_rating: {}".format(existent_rating.id))
                                        except Rating.DoesNotExist:
                                            existent_rating = None
                                        if existent_rating:
                                            if existent_rating.reviews != no_reviews:
                                                if existent_rating.stars > no_of_stars:
                                                    rating_is_negative = True
                                                q = (Rating.update({
                                                    Rating.is_negative: rating_is_negative,
                                                    Rating.stars: no_of_stars,
                                                    Rating.reviews: no_reviews,
                                                    Rating.date_last_review:datetime.datetime.now()
                                                }).where(Rating.id == existent_rating.id))
                                                q.execute()  # Execute the query, returning number of rows updated
                                        else:
                                            rating = Rating(
                                                product=existent_product,
                                                stars=no_of_stars,
                                                reviews=no_reviews,
                                                is_negative=False,
                                                date_last_review=datetime.datetime.now()
                                            )
                                            rating.save()


                                        last_price = Price(
                                            product=existent_product,
                                            date_added=datetime.datetime.now(),
                                            value=pret)

                                        if log_level == 'DEBUG':
                                            if k % 10 == 0:
                                                try:
                                                    print('#352# produsu e {} cu pret {}'.format(existent_product.title, last_price.value))
                                                except:
                                                    titlu_produs = produs.encode('ascii', 'ignore')
                                                    print('cannot print db values for product: {}'.format(titlu_produs))

                                        if pret != existent_product.price:
                                            print("#357 pret != existent_product.price")
                                            log = u"\n{} the product with ID_type {} and ID {}, from {} category, has a different price: \n{} coming from {}".format(
                                                datetime.datetime.now(), type(existent_product.id), existent_product.id,
                                                existent_category.name, pret, existent_product.price
                                            )
                                            discount_logging_filename = 'preturiada-' + datetime.datetime.now().strftime(
                                                '%m-%d') + '.log'
                                            try:
                                                loggingLogic(discount_logging_filename, log.encode("utf8"))
                                                print(log.encode("utf8"))
                                            except Exception as e:
                                                print("# exception at line 328 ", e)
                                            discount = round(pret - existent_product.price, 3)
                                            if discount < 0:
                                                is_negative = True

                                            prod_discount = Discount(
                                                product=existent_product,
                                                price=pret,
                                                discount=discount,
                                                is_negative=is_negative,
                                                date_added=datetime.datetime.now()
                                            )
                                            prod_discount.save()

                                            prod_rating = Rating(
                                                product=existent_product,
                                                stars=no_of_stars,
                                                reviews=no_reviews,
                                                is_negative=False,
                                                date_last_review=datetime.datetime.now()
                                            )

                                            existent_product.price = pret
                                            last_price.save()
                                            prod_rating.save()

                                        existent_product.category = existent_category
                                        existent_product.save()
                                except Exception as e:
                                    print(j, "# exception at line 357 ", e, "\n TRACEBACK is: ")
                                    ex_type, ex, tb = sys.exc_info()
                                    traceback.print_tb(tb)
                                    # screenshot_title = str(datetime.datetime.now())+'#'+str(produs.encode("utf8")+".png")
                                    # print(screenshot_title)
                                    # driver.save_screenshot(screenshot_title)
                                    continue
                if products == "different_url":
                    # break
                    print("#361 different url")

                if j%10==1:
                    print(['####################### flashDeals Logic here'])
                    campaign = flashDealsLogic(base_url)
                    print(["#302 starting flashCreatorLogic[", campaign, log_level])
                    flashProducts = flashCreatorLogic(campaign, log_level) # None
                    if flashProducts is None:
                        print("#no flash")
                    else:
                        print(["#306 no of returned products: ", len(flashProducts.keys())])
                        for l, produs in enumerate(flashProducts.keys()):
                            is_negative = False
                            if l == 2:
                                print("#flash DB Logic for: ", l, produs)
                            if log_level == "DEBUG":
                                if l % 10 == 0:
                                    print(["#flash DB Logic 4, ", l, "th product"])
                            pret = float(flashProducts[produs][0])
                            if flashProducts[produs][1].endswith('/'):
                                link = unicode(flashProducts[produs][1])
                            else:
                                link = unicode(flashProducts[produs][1] + '/')

                            try:
                                existent_product = Product.select().where(
                                    Product.title == produs
                                ).get()
                                print("### product is in DB: ", produs)
                            except Product.DoesNotExist:
                                print("### notinDB Exc@324, product is not here: ", produs)
                                existent_product = None
                            if existent_product is None:
                                if link.endswith('//'):
                                    link = link[:-1]
                                try:
                                    existent_product_link = Link.select().where(
                                        Link.link == link
                                    ).get()
                                except Link.DoesNotExist:
                                    print("### notinDB Exc@338, product link is not here: ", produs)
                                    existent_product_link = None

                                if existent_product_link:
                                    print(["#342 product with this link exists: ", link])
                                    existent_product = Product.select().where(
                                        Product.id == existent_product_link.product_id
                                    ).get()
                                else:
                                    continue
                                    # FIXME: here I cannot create a new product because I dunno the category
                            else:
                                try:
                                    existent_product_link = Link.select().where(
                                        Link.link == link,
                                        Link.product_id == existent_product.id
                                    ).get()
                                except Link.DoesNotExist:
                                    print("### notinDB Exc@356, product link is not in DB: ", produs)
                                    existent_product_link = None
                                if existent_product_link is None:
                                    link_url = Link(
                                        product=existent_product,
                                        link=link)
                                    try:
                                        query = Link.update(link=link).where(
                                            Link.product_id == existent_product.id
                                        )
                                        query.execute()
                                        #  link_url.save()
                                    except Exception as e:
                                        print(['###EXC@369 cannot save new link: ', e])
                            if existent_product:
                                last_price = Price(
                                    product=existent_product,
                                    date_added=datetime.datetime.now(),
                                    value=pret)
                                try:
                                    if existent_category:
                                        continue
                                except:
                                    existent_category = existent_product.category
                                if pret != existent_product.price:
                                    log = u"\n{} the product with ID_type {} and ID {}, from {} category, has a different price: \n{} coming from {}".format(
                                        datetime.datetime.now(), type(existent_product.id), existent_product.id,
                                        existent_category.name, pret, existent_product.price
                                    )
                                    discount_logging_filename = 'preturiada-' + datetime.datetime.now().strftime(
                                        '%m-%d') + '.log'
                                    try:
                                        loggingLogic(discount_logging_filename, log.encode("utf8"))
                                        print(log.encode("utf8"))
                                    except Exception as e:
                                        print("# exception@381 ", e)
                                    discount = round(pret - existent_product.price, 3)
                                    if discount < 0:
                                        is_negative = True
                                    prod_discount = Discount(
                                        product=existent_product,
                                        price=pret,
                                        discount=discount,
                                        is_negative=is_negative,
                                        date_added=datetime.datetime.now()
                                    )
                                    prod_discount.save()

                                    existent_product.price = pret
                                    last_price.save()

                                existent_product.save()




    try:
        run_time = datetime.datetime.now() - start_time
        final_msg = u"\n YOUR script has ended in {} \n for these links:\n {}".format(
            str(divmod(run_time.days * 86400 + run_time.seconds, 60)), urlList)
        emailLogic(final_msg.encode("utf8"),['bogdan.tam@me.com'])
    except Exception as e:
        print("# email sender fail", e)
