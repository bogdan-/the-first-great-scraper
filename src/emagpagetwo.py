import traceback

from src.creatorpagetwo import *
from utils.dbLogic import *
from utils.popupBlocker import popupBlocker


def seleniumLogic(driver, url, max_page_depth, log_level):

    start_time = datetime.datetime.now()

    print([start_time, "and pages to scan: ", max_page_depth])
    app_url = "http://www.preturi.date/index.php?r=product%2F"
    base_url = "https://www.emag.ro"
    namesForFashion = ["imbracaminte", "fashion", "fossil", "Ceasuricopii", "antivirus", "Accesoriiorga",
                       "maternitate", "gravide", "motocultoare", "tonere", "proiectie", "portjartiere",
                       "alimente", "alimente-sanatoase", "alimentesanatoase","lanturi-auto","scannere-3d"
                       ]
    unwantedCategoriesList = ["supermarket"]
    desiredList = [1, 293, 60, 65, 147
                   ]
    print("# Selenium logic starts here with DRIVER navigation to", url)

    # navigate to the application home page
    driver.get(url)
    urlList = []  # list of "/department/c" type strings
    tableNameList = []
    departsWebElemListe = []  # list of webdriver.remote.webelement.WebElement elements
    cheap_products = []
    discount_of = {}
    urls_file = open('url_file.txt', 'w')
    depts_file = open('depts_file.txt', 'w')

    # get the list of department elements
    try:
        print("# getting the list of elements")
        departmentsPage = driver.find_elements_by_id(
            "departments-page")
        for dep in departmentsPage:
            departments = dep.find_elements_by_id(
                "department-expanded")
            for depExpanded in departments:
                departmentsExpanded = depExpanded.find_elements_by_tag_name(
                    "li")
                # print("#new# departments Expanded: ", len(departments))
                for s, subdept in enumerate(departmentsExpanded):
                    subdepartment = subdept.find_elements_by_tag_name(
                        "li")
                    for i, el in enumerate(subdepartment):  # here el is a webdriver.remote.webelement.WebElement
                        print(["#...",s,i])

                        departsWebElemListe.append(el)  # list of webdriver.remote.webelement.WebElement elements
                        html = el.get_attribute('outerHTML')
                        attrs = BeautifulSoup(html, 'html.parser').a.attrs
                        url = attrs["href"]
                        urlSplitted = url.split("?")[0][:-1]  # split the url and then delete the last character (/c) from the url

                        if not [m for m in unwantedCategoriesList if m in urlSplitted]:
                            urlList.append(urlSplitted)
                        else:
                            continue
                    #     if i>10 and s>10:
                    #         break
                    # if s>10:
                    #     break

                        # print(len(urlList), urlSplitted)
        urls_file.write("%s\n" % urlList)
        depts_file.write("%s\n" % departsWebElemListe)
    except Exception as e:
        print("# Exception at line 56", e)
        ex_type, ex, tb = sys.exc_info()
        traceback.print_tb(tb)
    finally:
        depts_file.close()
        urls_file.close()

    # get the number of elements found
    # print("# departments Expanded: ", len(departmentsListe))

    # iterate through each element and print the text that is
    print("# parsing the list")

    for i, department in enumerate(departsWebElemListe):
        # if i > 100:
        #     break
        if len(department.text) > 80:
            print('!@#$%^&**&^%$#@! ', department.text)
            continue
        else:
            if i == 1 or i == 100 or i == 1000:
                print("# dep names")
                print(i, tableName)
            name = department.text
            tableName = ''.join(e for e in name if e.isalnum())
            if not [m for m in unwantedCategoriesList if m in tableName]:
                tableNameList.append(tableName)
            else:
                continue

############################################################### $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    # for j, departmentName in enumerate(tableNameList[975:], start=975):
    # for j, departmentName in reversed(list(enumerate(tableNameList))):
    for j, departmentName in enumerate(tableNameList):
        print(["j = ", j])
        if j > len(urlList)-1:
            continue
        else:
            if [n for n in namesForFashion if n in departmentName or n in urlList[j]]:  #list comprehension made by me
                print("#103 entered List comprehention for: ", urlList[j])
                continue
            else:
                if log_level == "DEBUG":
                    print("#106 department name is: ", departmentName, urlList[j], tableNameList[j])
                print("# fileCreator Logic")
                popupBlocker(driver)
                for x in range(1,max_page_depth):
                    if x == 1:
                        link_url = urlList[j] + 'c'
                    else:
                        link_url = urlList[j] + 'p' + str(x) + '/c'  # if multiple pages needed to follow add: + "p2/c" ...
                    products = fileCreatorLogic(base_url + link_url, driver, log_level)  # here you add the url page if you need a specific one
                    category_link = base_url + link_url

                    if products is None:
                        print("#127 EXITING: products was none")
                        # break  # break here if fileCreatorLogic fails to return something in 'products' attr
                    else:
                        print(["# no of returned products: ", len(products)])
                        for k, produs in enumerate(products.keys()):
                            is_negative = False
                            if k == 2:
                                print("#DB Logic for: ", k)
                            if log_level == "DEBUG":
                                if k%10 == 0:
                                    print(["# DB Logic 4, ", k, "th product"])
                            pret = float(products[produs][0])
                            link = products[produs][1]

                            # exportFile = open(departmentName, "w")
                            # exportFile.write(str(products))
                            # exportFile.close()
                            ######################################
                            try:

                                try:
                                    existent_category = Category.select().where(
                                        Category.name == departmentName
                                    ).get()
                                except Category.DoesNotExist:
                                    existent_category = None

                                if existent_category is None:
                                    existent_category = Category.create(name=departmentName, url=category_link)

                                if existent_category.url == None:
                                    existent_category.url = category_link

                                    existent_category.save(only=[Category.url])

                                try:
                                    existent_product = Product.select().where(
                                        Product.title == produs
                                    ).get()
                                except Product.DoesNotExist:
                                    # print("### Exception at line 140, product is not here: ", produs)
                                    existent_product = None
                                try:
                                    existent_product_link = Link.select().where(
                                        Link.link == link
                                    ).get()
                                except Link.DoesNotExist:
                                    # print("### Exception at line 140, product is not here: ", produs)
                                    existent_product_link = None

                                if existent_product_link and existent_product==None:
                                    existent_product = Product.select().where(
                                        Product.id == existent_product_link.product_id
                                    ).get()

                                if existent_product is None:
                                    product_one = Product(
                                        category=existent_category,
                                        title=produs,
                                        date_added=datetime.datetime.now(),
                                        price=pret)
                                    last_price = Price(
                                        product=product_one,
                                        date_added=datetime.datetime.now(),
                                        value=pret)
                                    link_url = Link(
                                        product = product_one,
                                        link = link)
                                    product_one.save()
                                    last_price.save()
                                    try:
                                        link_url.save()
                                    except Exception as e:
                                        print("# exception at link saving, line 158 ",link, e, "\n TRACEBACK is: ")
                                        ex_type, ex, tb = sys.exc_info()
                                        traceback.print_tb(tb)
                                        log_msg = u"{} the product {} found here: {}, exception: {}, traceback: {}\n \n".format(
                                            datetime.datetime.now(), produs, link, e, traceback.print_tb(tb)
                                        )
                                        loggingLogic('logging_for_link_constraint.log', log_msg.encode("utf8"))
                                        # screenshot_title = str(datetime.datetime.now()) + '#' + str(produs.encode("utf8")+".png")
                                        # driver.save_screenshot(screenshot_title)
                                        continue

                                else:
                                    last_price = Price(
                                        product=existent_product,
                                        date_added=datetime.datetime.now(),
                                        value=pret)

                                    if pret != existent_product.price:
                                        log = u"\n{} the product with ID_type {} and ID {}, from {} category, has a different price: \n{} coming from {}".format(
                                            datetime.datetime.now(), type(existent_product.id), existent_product.id, existent_category.name, pret, existent_product.price
                                        )
                                        discount_logging_filename = 'preturiada-' + datetime.datetime.now().strftime('%m-%d') + '.log'
                                        try:
                                            loggingLogic(discount_logging_filename, log.encode("utf8"))
                                        except Exception as e:
                                            print("# exception at line 226 ", e)
                                        discount = round(pret - existent_product.price, 3)
                                        if discount < 0:
                                            is_negative = True

                                        # if existent_product.category_id in desiredList and pret < existent_product.price*0.75:
                                        #     cheaper_by = existent_product.price - pret
                                        #     url_of_cheaper_product = app_url + "view&id=" + str(existent_product.id)
                                        #     msg = u"\n {} has a new price: \n {} \n coming down from: \n {} being cheaper by {}" \
                                        #           u"\n More details here: {}".format(
                                        #         existent_product.title, pret, existent_product.price, cheaper_by,
                                        #         url_of_cheaper_product)
                                            # notifyLogic(existent_product, pret, msg)
                                        # if pret < existent_product.price*0.75:
                                        #     cheap_products.append([existent_product, round(pret - existent_product.price, 3)])
                                        #     discount_of[existent_product] = round(pret - existent_product.price, 3)


                                        prod_discount = Discount(
                                            product = existent_product,
                                            price = pret,
                                            discount = discount,
                                            is_negative = is_negative,
                                            date_added=datetime.datetime.now()
                                        )
                                        prod_discount.save()

                                        existent_product.price = pret
                                        last_price.save()

                                    existent_product.category = existent_category
                                    existent_product.save()
                            except Exception as e:
                                print(j, "# exception at line 259 ", e, "\n TRACEBACK is: ")
                                ex_type, ex, tb = sys.exc_info()
                                traceback.print_tb(tb)
                                # screenshot_title = str(datetime.datetime.now())+'#'+str(produs.encode("utf8")+".png")
                                # print(screenshot_title)
                                # driver.save_screenshot(screenshot_title)
                                continue
    # cheapestLogic(cheap_products, discount_of)
    try:
        run_time = datetime.datetime.now() - start_time
        final_msg = u"\n YOUR script has ended in {} \n for these links:\n {}".format(
            str(divmod(run_time.days * 86400 + run_time.seconds, 60)), urlList)
        # emailLogic(final_msg.encode("utf8"),['bogdan.tam@me.com'])
    except Exception as e:
        print("# email sender fail", e)
        driver.quit()
    try:
        driver.quit()
    except Exception as e:
        print("#EOF cannot quit driver at the end: ", e)




'''
from dbLogic import *
import datetime

for count in range(1,5):
        list_of_prices = []
        list_of_discounts = []
        for i in range(1,82001):
                list_of_prices = Price.select().where(Price.product_id == i)
                list_of_discounts = Discount.select().where(Discount.product_id == i)
                list = []
                list_of_old = []

                for j, price in enumerate(list_of_prices):
                    list.append(price)
                    if price.date_added < datetime.datetime.now() - datetime.timedelta(days=19):
                        list_of_old.append(price)

                print(['inainte: ',len(list_of_old), len(list)])
                for j, price in enumerate(list_of_old):
                    print('id: ',j)
                    try:
                        if price.value == list_of_old[j+2].value and list_of_old[j+4].value == list_of_old[j+2].value:
                            if j>1:

                                list_of_old.pop()
                                del list[j]
                                q = Price.delete().where(Price.id == price.id)
                                q.execute()
                    except Exception as e:
                        continue
        
        for j,discount in enumerate(list_of_discounts):
            if j < len(list_of_discounts)-3:
                q = Discount.delete().where(Discount.id == discount.id)
                q.execute()
'''