from selenium import webdriver
import time

login_url = "https://login.profitshare.ro/"
print("starting driver: ")
driver = webdriver.PhantomJS()
driver.set_window_size(1280,1080)
print("opening the page: ")
driver.get(login_url)
driver.implicitly_wait(10)

print("starting logic")
try:
    email = driver.find_element_by_css_selector('input[type=email]')
    password = driver.find_element_by_css_selector('input[type=password]')
    login = driver.find_element_by_css_selector('button[type="submit"]')
except Exception as e:
    print("driver didnt find element: ", e)
print("doing the login:")
email.send_keys('bogdantamirjean@gmail.com')
password.send_keys('liteon')
login.click()
print("after the click, sleep and screenshot")
time.sleep(2)
driver.get_screenshot_as_file('main.png')
