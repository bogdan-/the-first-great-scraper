import peewee

database = peewee.SqliteDatabase("wee.db")


class Category(peewee.Model):
    """
    ORM model of the Category table
    """
    try:
        name = peewee.CharField(unique=True)
    except Exception as e:
        print("db exception is: ", e)
    class Meta:
        database = database


class Product(peewee.Model):
    """
    ORM model of Product table
    """
    category = peewee.ForeignKeyField(Category)
    title = peewee.CharField(unique=True)
    date_added = peewee.DateTimeField()
    price = peewee.FloatField()

    class Meta:
        database = database

class Price(peewee.Model):
    """
    ORM model of the Category table
    """
    try:
        product = peewee.ForeignKeyField(Product)
        date_added = peewee.DateTimeField()
        value = peewee.FloatField()

    except Exception as e:
        print("db exception is: ", e)
    class Meta:
        database = database

class Link(peewee.Model):
    """
    ORM model of the Category table
    """
    try:
        product = peewee.ForeignKeyField(Product)
        link = peewee.CharField(unique=True)

    except Exception as e:
        print("db exception is: ", e)
    class Meta:
        database = database


if __name__ == "__main__":
    try:
        Category.create_table()
    except peewee.OperationalError:
        print "Category table already exists!"

    try:
        Product.create_table()
    except peewee.OperationalError:
        print "Product table already exists!"

    try:
        Price.create_table()
    except peewee.OperationalError:
        print "Price table already exists!"

    try:
        Link.create_table()
    except peewee.OperationalError:
        print "Link table already exists!"