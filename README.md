# Web Scraping Script with Selenium
This Python script provides logic for web scraping using Selenium. It navigates through web pages, collects data, and performs actions. Here’s how you can use it with the provided runner.py:
## Usage

### Prerequisites
Make sure you have Python and Selenium installed on your system.

### Install Dependencies
- A `requirements.txt` file is provided. Install the required packages by running:
pip install -r requirements.txt

### Set Up Parameters
- Open `runner.py`.
- Set the `url` parameter to the desired starting URL.
- Adjust other parameters (e.g., `max_page_depth`, `log_level`, etc.) as needed.

### Database Configuration
We’ll use Peewee ORM to manage our database. The provided models (Category, Product, Price, and Link) define the schema. Here’s how to set up the database:
Ensure you have SQLite installed.
Create a new SQLite database file (e.g., wee.db).
Update the database variable

### Run the Script
Execute the following command in your terminal:
python runner.py NO_OF_PAGES LOG_LEVEL
Replace `NO_OF_PAGES` with the desired number of pages to scan (default is 2 if not specified). Set `LOG_LEVEL` to control the verbosity of logging (optional).

### Script Execution
- The script initializes a PhantomJS WebDriver.
- It navigates to the specified URL.
- The `seleniumLogic` function is executed.
- Collected URLs are saved to `url_file.txt`, and department details are saved to `depts_file.txt`.

### Database Schema
#### Category
Represents categories for scraped data.
Unique field: name.
#### Product
Represents product details.
Fields: category, title, date_added, and price.
#### Price
Represents historical price data.
Fields: product, date_added, and value.
#### Link
Represents URLs associated with products.
Unique field: link.

Feel free to customize and expand upon this logic for your specific use case. Happy scraping! 🕷️🌐